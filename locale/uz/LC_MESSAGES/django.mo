��    /      �  C                   "     /     7  	   =     G  
   O     Z  "   y     �     �     �     �     �     �          9     J     [  U   d     �     �     �     �  
           &   3     Z  4   k  $   �  (   �  )   �  0        I     h     �     �  �   �  (   8  #   a     �     �  c   �     	  C   	     c	  ;  v	     �
     �
     �
     �
     �
     �
          	     $     7     @     M     Y     l     |     �     �     �     �  4   �     �     �                -     5     >  	   T  $   ^     �     �     �     �     �     �     �     
  T        e     �     �  	   �  0   �     �  ,        1     &                            !                                  /   *                 -   ,          '                                   (                        
   "         .      #       +               	             $      %   )    Messages Phone Number Russian Uzbek Your Name message Адрес ВАША МЕЧТА ЗДЕСЬ Ваш номер телефона Ваше имя Время работы Главная График работы Заказать Заказать звонок Как мы работаем Качество Контакты Меню Мы рады слышать удовлетворение наших клиентов Наши Работы Наши работы О компании Оперативность Отзыв Отправить Оцените свою площадь Партнеры Подать заявку на нашу услугу Получите свое право Понедельник - Суббота: Понедельник - Суббота:  Посмотреть больше отзывов Построй свой дом Преимущества Приемлемые цены Связаться Спасибо, что связались с нами. Один из наших коллег скоро свяжется с вами! Строим будущее вместе Телефон и Эл. почта: Узнать больше Услуги г. Ташкент, Юнусабадский район, ул. Чингиза Айтматова 54 главная мы всегда заботимся о наших клиентах с 9.00 до 17.00 Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Sizning Xabaringiz Telefon raqamingiz Ruscha O'zbek Ismingiz Sizning Xabaringiz Manzil Sizning orzuingiz shu erda Telefon raqamingiz Ismingiz Ish vaqtimiz Bosh sahifa Bizning ishlarimiz Buyurtma qilish Qo'ng'iroq qiling Qanday ishlayapmiz? Sifat Aloqa Menyu Mijozlarimizning mamnunligini eshitishdan xursandmiz Bizning ishlarimiz Bizning ishlarimiz Kompaniya haqida Samaradorlik Fikrlar yuborish Hududingizni baholang Hamkorlar Bizning xizmatimizga murojaat qiling Hududingizni baholang Dushanba - shanba: Dushanba - shanba: Boshqa sharhlarni ko'ring Uyingizni quring Foyda Qabul qilinadigan narxlar Aloqa Biz bilan bog'langaningiz uchun tashakkur. Biz siz bilan tez orada aloqaga chiqanmiz Kelajakni birgalikda quramiz Telefon va elektron pochta Qo'shimcha ma'lumot olish uchun Xizmatlar Toshkent, Yunusobod tumani, Chingiz Aytmatova 54 Bosh sahifa biz doimo mijozlarimizga g'amxo'rlik qilamiz 9.00 dan 17.00 gacha 