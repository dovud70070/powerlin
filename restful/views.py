from django.template.loader import render_to_string
from rest_framework.response import Response
from rest_framework.views import APIView

from app.models import Message


class SendMessage(APIView):
    def post(self, request):
        data = request.POST.copy()
        message = Message(your_name=data['name'], phone_number=data['phone'])
        message.save()
        html = render_to_string("restful/message.html")
        return Response({'success': html})
