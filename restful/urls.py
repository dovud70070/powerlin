from django.urls import path
from .views import *
app_name='rest'

urlpatterns = [
    path('send/', SendMessage.as_view(), name='send_message')
]