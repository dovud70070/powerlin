$(document).ready(function () {
  $(".hamburger").click(function () {
    $(this).toggleClass("close-hamburger");
    $(".header__menu ul").toggleClass("active");
    $("body").toggleClass("ovh");
  });

  $('[data-fancybox]').fancybox({
  thumbs : {
    autoStart : true,
  }




})

  $(".header__btn").on("click", function () {
    $(".form-popup").addClass("active");
  });

  $(".main-services__content-btn a").on("click", function () {
    $(".form-popup").addClass("active");
  });

  $(".form-block__close").on("click", function () {
    $(".form-popup").removeClass("active");
  });

  $(".main-slider").slick({});
// $(".form-block__btn").on("click", function () {
//   // if($('.form-block__input input').val() === '') {
//   //   return
//   // }
//   console.log('bosildi')
//
//
//
//     $(".form-success-content").addClass("active");
//     setTimeout(function(){
//
//       $(".form-success-content").removeClass("active");
//       $(".form-popup").removeClass("active");
//     },4000)
//
//
//   });

  $(".testimonials__slider").slick({
    // normal options...
    slidesToShow: 2,
    // the magic
    responsive: [
      {
        breakpoint: 1442,
        settings: {
          slidesToShow: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 775,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
  $(".our-works__slider").slick({
    // normal options...
    variableWidth: true,
    infinite: true,
    centerMode: true,
    arrows: false,
    dots: true,
    // the magic
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          dots: true,
        },
      },
    ],
  });
  $(".partners__slider").slick({
    // normal options...
    slidesToShow: 4,
    infinite: true,

    // the magic
    responsive: [
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 3,
          infinite: true,
        },
      },
      {
        breakpoint: 775,
        settings: {
          slidesToShow: 2,
          infinite: true,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          infinite: true,
        },
      },
    ],
  });
});
