from django.contrib import admin
from .models import Service, AboutCompany, Message, Testimonial, ServiceSpecification, MainSlider, OurWorks, \
    OurWorksImages
from parler.admin import TranslatableAdmin


class ServiceInline(admin.StackedInline):
    model = ServiceSpecification
    fields = ['info']
    readonly_fields = ['info']


@admin.register(Service)
class ServiceAdmin(TranslatableAdmin):
    list_display = ['title']
    inlines = [ServiceInline, ]


@admin.register(ServiceSpecification)
class ServiceSpecificationAdmin(TranslatableAdmin):
    list_display = ['info']


@admin.register(AboutCompany)
class AboutCompanyAdmin(TranslatableAdmin):
    list_display = ['title', 'slug']

    def get_prepopulated_fields(self, request, obj=None):
        return {'slug': ('title',)}


@admin.register(Testimonial)
class TestimonialAdmin(TranslatableAdmin):
    list_display = ['name']


@admin.register(MainSlider)
class MainAdmin(TranslatableAdmin):
    list_display = ['title']


class OurWorksInlineAdmin(admin.StackedInline):
    model = OurWorksImages
    fields = ['image', ]


class OurWorksAdmin(TranslatableAdmin):
    list_display = ['title']
    inlines = [OurWorksInlineAdmin, ]


admin.site.register(Message)
admin.site.register(OurWorks, OurWorksAdmin)
