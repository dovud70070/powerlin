from django.urls import path
from .views import IndexView, ContactView, AboutView, ServiceView, OurWorksView,submit_form, TestimonialView


app_name = 'app'

urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('contact/', ContactView.as_view(), name='contact'),
    path('service/', ServiceView.as_view(), name='service'),
    path('our_works/', OurWorksView.as_view(), name='our_works'),
    path('submit/form/', submit_form, name='submit_common_form'),
    path('testimonial/', TestimonialView.as_view(), name='testimonial'),
    path('about/<slug:slug>/', AboutView.as_view(), name='about'),
]