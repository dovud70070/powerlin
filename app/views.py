from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .models import Service, Testimonial, MainSlider, OurWorks, AboutCompany
from .forms import MessageForm


class IndexView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['services'] = Service.objects.all()
        context['message_form'] = MessageForm()
        context['testimonials'] = Testimonial.objects.all()
        context['main_slider'] = MainSlider.objects.all()
        context['our_works'] = OurWorks.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        context = {}
        message_form = MessageForm(data=request.POST or None)
        if message_form.is_valid():
            message_form.save()
            messages.success(request=self.request, message=('successfully',))
        else:
            context['message_form'] = MessageForm()
        return render(request, self.template_name, self.get_context_data(**context))


def submit_form(request):
    if request.method == 'POST':
        message_form = MessageForm(data=request.POST or None)
        if message_form.is_valid():
            message_form.save()
            messages.success(request=request, message=('successfully',))
            return redirect(request.META.get('HTTP_REFERER'))
        return redirect(request.META.get('HTTP_REFERER'))


class ContactView(TemplateView):
    template_name = 'contacts.html'


class AboutView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            about_data = AboutCompany.objects.get(slug=kwargs['slug'])
        except ModuleNotFoundError:
            about_data = {}
        context['about_data'] = about_data
        return context


class ServiceView(TemplateView):
    template_name = 'service.html'


class OurWorksView(TemplateView):
    template_name = 'ourWorks.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['our_works'] = OurWorks.objects.all()
        return context


class TestimonialView(TemplateView):
    template_name = 'testimonials.html'
