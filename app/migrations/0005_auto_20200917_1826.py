# Generated by Django 3.1.1 on 2020-09-17 13:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_aboutcompany_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutcompany',
            name='image',
            field=models.ImageField(blank=True, height_field=12, null=True, upload_to='', width_field=12),
        ),
    ]
