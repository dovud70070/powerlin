from django import forms
from .models import Message, Service
from django.utils.translation import gettext_lazy as _


class MessageForm(forms.ModelForm):
    message = forms.CharField(required=False)

    class Meta:
        model = Message
        fields = ['your_name', 'phone_number', 'message']
        widgets = {
            'your_name': forms.TextInput({'placeholder': _('Your Name')}),
            'phone_number': forms.TextInput({'placeholder': _('Phone Number')}),
            'message': forms.TextInput({'placeholder': _('Message')}),
        }

