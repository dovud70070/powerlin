from .forms import MessageForm
from .models import Service, OurWorks, AboutCompany, Testimonial


def common_data(request):
    context = {
        'common_form': MessageForm,
        'services': Service.objects.all(),
        'our_works': OurWorks.objects.all(),
        'about_company': AboutCompany.objects.all(),
        'testimonials': Testimonial.objects.all(),
    }
    return context
