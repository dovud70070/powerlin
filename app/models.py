from django.db import models
from django.urls import reverse
from django.views import generic
from parler.models import TranslatableModel, TranslatedFields
from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from filer.fields import image as filer_image


class MainSlider(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=200, null=True, blank=True),
        description=RichTextUploadingField(null=True, blank=True),
    )
    image = models.ImageField(upload_to='media/%Y/%m/d')

    def __str__(self):
        return self.safe_translation_getter('title' or None)


class Testimonial(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=200, blank=True),
        description=RichTextUploadingField(null=True, blank=True),
        position=models.CharField(max_length=200)
    )
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='media/%Y/%m/%d')
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.safe_translation_getter('title')


class AboutCompany(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=200, null=True, blank=True, verbose_name='Title'),
        description=RichTextUploadingField(null=True, blank=True)
    )
    slug = models.SlugField(max_length=200, unique=True)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.safe_translation_getter('title' or '')

    class Meta:
        verbose_name = 'About Company'


class Service(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=100, verbose_name='Title'),
        description=RichTextUploadingField(null=True, blank=True)
    )
    image = models.ImageField(upload_to='media', null=True, blank=True)

    def __str__(self):
        return "{}".format(self.safe_translation_getter('title') or '')

    class Meta:
        verbose_name = 'Service'


class ServiceSpecification(TranslatableModel):
    translation = TranslatedFields(
        info=RichTextUploadingField(null=True, blank=True)
    )
    service = models.ForeignKey(Service, on_delete=models.CASCADE, related_name='specification')

    def __str__(self):
        return self.safe_translation_getter('info' or None)


class OurWorks(TranslatableModel):
    translation = TranslatedFields(
        title=models.CharField(max_length=200, blank=True, null=True),
        description=models.TextField(),
    )
    main_image = filer_image.FilerImageField(null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.safe_translation_getter('title' or None)


class OurWorksImages(models.Model):
    our_work = models.ForeignKey(OurWorks, on_delete=models.CASCADE, related_name='image_list')
    image = filer_image.FilerImageField(null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return "{}".format(self.our_work.safe_translation_getter('title'))


class Message(models.Model):
    your_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.your_name
